function EB_PV = targetFun(TemperatureC)

%% Script that calculates the module temperature of CPV modules
% after Dobos 2019: An Energy Balance Model for CPV Module Lens and Cell
% temperature

%% Constants
A_lens = 0.07;
A_cell = 0.0001;                    % Solar cell area (1 cm2)
X = A_lens/A_cell;                  % concentration ratio
beta = 30;                          % module tilt angle (°)
Pr = 0.71;                          % Prandtl number for air
k = 0.025;                          % thermal conductivity of air (W m-1 K-1)
v_kin = 17.7*10^(-6);               % kinematic viscosity of air (m^2 s^-1)
beta_int = 90;                      % angle for natural convection in enclosure (°)
Lc = 10*sqrt(A_lens);
L_focal = 10*sqrt(A_lens);          % focal length
F_cell_lens = 1;                    % view factor of the cell to the upper surface assumming A_cell << A_lens
F_lens_cell = F_cell_lens * A_cell / A_lens; % view factor of the lens to the cell
F_lens_sky = (1 + cosd(beta))/2;    % view factor of the upper surface to the sky
F_lens_gnd = 1 - F_lens_sky;        % view factor of the upper surface to the ground
F_back_gnd = (1 + cosd(beta))/2;    % view factor of the lower surface to the ground
F_back_sky = 1 - F_back_gnd;        % view factor of the lower surface to the sky
F_back_lens = 0.199;                % view factor of the lower to the upper surface (?)

Eff_PV = 0.39;                      % cell efficiency - dependency with temp. currently missing - needs refinement
epsilon_cell = 0.85;                % cell emissivity
epsilon_lens = 0.84;                % lens emissivity
epsilon_back = 0.1;                 % backplate emissivity
k_epoxy = 7;                        % epoxy conductivity (W m-1 K-1)
l_epoxy = 0.00025;                  % epoxy thickness (m)
l_lens = 0.003;                     % lens thickness (m)
K_lens = 4;                         % lens extinction
sigma = 5.67*10^-8;                 % Stefan Boltzmann constant (W m-2 K-4) 

% Meteorological variables
G_incident = 900;       % global radiation (W m-2) - this needs to be refined for total shortwave rad. calculation
T_amb = 20+273.15;      % ambient air temperature (K)
u = 1;                  % horizontal wind speed (m s-1)

T_lens = TemperatureC(1); % lens temperature
T_cell = TemperatureC(2); % cell temperature
T_back = TemperatureC(3); % backplate temperature

TemperatureC

% CPV model
T_sky = 0.0522*T_amb^1.5;

alpha = 1 - exp(-K_lens*l_lens);    % absorptivity lens
tau = exp(-K_lens*l_lens);          % transmissivity lens

T_ground = (T_lens + T_amb)/2; % rough guess of ground temperature - needs refinements

T_ave_l = (T_lens + T_amb)/2; % used to calculate natural convection of lens
T_ave_b = (T_back + T_amb)/2; % used to calculate natural convection of backplate

Ra_l = abs(1.162 * 10^19 * T_ave_l^(-4.47) * sind(beta) * (T_lens - T_amb) * Lc^3); % Rayleigh number near lens
Ra_b = abs(1.162 * 10^19 * T_ave_b^(-4.47) * sind(beta) * (T_back - T_amb) * Lc^3); % Rayleigh number near backplate

Nu_nat_l = (0.825 + (0.387*Ra_l^(1/6)/((1 + (0.492/Pr )^(9/16))^(8/27))))^2; % natural convection lens to air
Nu_nat_b = (0.825 + (0.387*Ra_b^(1/6)/((1 + (0.492/Pr )^(9/16))^(8/27))))^2; % natural convection backplate to air

hnat_l = (Nu_nat_l * k) / Lc; % natural convection heat transfer coefficient for lens to air
hnat_b = (Nu_nat_b * k) / Lc; % natural convection heat transfer coefficient for backplate to air

Re = u * Lc / v_kin; % Reynolds number

Nu_for = 0.037*Re^0.8*Pr^(1/3);     % forced convection

T_ave = (T_lens + T_back) / 2;

Ra = abs(1.162 * 10^19 * T_ave^(-4.47) * sind(beta_int) * (T_lens - T_back) * L_focal^3); % Rayleigh number in enclosure

Nu_int = 0.046 * Ra^(1/3);     % natural convection in enclosure

hint = (Nu_int * k) / L_focal;

hfor = (Nu_for * k) / Lc; 

heff_l = (hnat_l^3 + hfor^3)^(1/3); % combined natural and forced convection heat transfer coefficient lens to air
heff_b = (hnat_b^3 + hfor^3)^(1/3); % combined natural and forced convection heat transfer coefficient backplate to air

hr_l_sky = (T_lens^2 + T_sky^2) * (T_lens + T_sky); % linearized radiation heat transfer coefficient
hr_l_gnd = (T_lens^2 + T_ground^2) * (T_lens + T_ground); % linearized radiation heat transfer coefficient
hr_b_sky = (T_back^2 + T_sky^2) * (T_back + T_sky); % linearized radiation heat transfer coefficient
hr_b_gnd = (T_back^2 + T_ground^2) * (T_back + T_ground); % linearized radiation heat transfer coefficient
hr_l_b = (T_lens^2 + T_back^2) * (T_lens + T_back); % linearized radiation heat transfer coefficient
hr_l_c = (T_lens^2 + T_cell^2) * (T_lens + T_cell); % linearized radiation heat transfer coefficient

q_conv_nat_for_l = heff_l * A_lens * (T_lens - T_amb); % natural + forced convection from the lens/upper surface
q_conv_nat_for_b = heff_b * A_lens * (T_back - T_amb);   % natural + forced convection from the backplate/lower surface

q_conv_nat_int_l = hint * A_lens * (T_lens - T_back);  % natural convection inside enclosure - eq. 7
q_conv_nat_int_b = hint * A_lens * (T_back - T_lens);  % natural convection inside enclosure - eq. 7

q_rad_sky_l = epsilon_lens * sigma * F_lens_sky * A_lens * hr_l_sky * (T_lens - T_sky); % total radiative heat transfer upper surface with sky
q_rad_sky_b = epsilon_back * sigma * F_back_sky * A_lens * hr_b_sky * (T_back - T_sky); % total radiative heat transfer lower surface with sky

q_rad_gnd_l = epsilon_lens * sigma * F_lens_gnd * A_lens * hr_l_gnd * (T_lens - T_ground);  % total radiative heat transfer upper surface with ground
q_rad_gnd_b = epsilon_back * sigma * F_back_gnd * A_lens * hr_b_gnd * (T_back - T_ground);  % total radiative heat transfer lower surface with ground

q_rad_cell = epsilon_lens * sigma * F_lens_cell * A_lens * hr_l_c * (T_lens - T_cell); % total radiative heat transfer upper surface with cell surface
q_rad_back = epsilon_lens * sigma * F_back_lens * A_lens * hr_l_b * (T_lens - T_back); % total radiative heat transfer upper surface with lower surface

q_rad_cell_lens = epsilon_cell * sigma * F_cell_lens * A_cell * hr_l_c * (T_cell - T_lens); % total radiative heat transfer cell with upper surface - eq. 10
q_rad_back_lens = epsilon_back * sigma * F_back_lens * A_lens * hr_l_b * (T_back - T_lens); % total radiative heat transfer back with upper surface

q_cond = (k_epoxy * A_cell)/l_epoxy * (T_cell - T_back); % conduction from cell through the epoxy to the backplate - eq. 11

% Energy balance of lens
EB_lens = alpha * G_incident * A_lens - q_conv_nat_for_l - q_conv_nat_int_l - q_rad_sky_l - q_rad_gnd_l - q_rad_cell - q_rad_back;

% Energy balance of cell
EB_cell = tau * G_incident * X * A_cell*(1 - Eff_PV)-q_rad_cell_lens-q_cond; % eq. 9

% Energy balance of backplate
EB_back = q_cond - q_conv_nat_int_b - q_conv_nat_for_b - q_rad_sky_b - q_rad_gnd_b - q_rad_back_lens; % eq. 12

EB_PV = [EB_lens EB_cell EB_back];

end