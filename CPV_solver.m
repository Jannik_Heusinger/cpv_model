% implement root finding algo for steady state solution with fzero

T_amb = 273.15 + 20;
% Initial guess for lens, cell and backplate temperatures
T_lens = T_amb; % lens temperature
T_cell = T_amb; % cell temperature
T_back = T_amb; % backplate temperature

TemperatureC = [T_lens T_cell T_back];
[x,fval] = fsolve(@targetFun,TemperatureC);